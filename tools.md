## Tools

* **[Install Docker]**
* **[Install GPG Tools]**
* **[Install git-crypt]**
* **[Install AWS CLI]**
* **[Install gcloud]**
* **[Install kubectl]**
* **[Install gsutil]**

* **[Install Jq]**

   jq is a lightweight and flexible command-line JSON processor

   yq is a jq wrapper for YAML documents


	**Tip:** for MacOS users:

   ```
   brew install jq
   pip install yq
   ```

 * **[Install Yq]**

   yq is a jq wrapper for YAML documents

	**Tip:** for MacOS users:

   ```
   pip install yq
   ```

* **Install gettext on MacOS**

	_envsubst_ is need to implement env based template

	```
	brew install gettext
	brew link --force gettext
	```
* **[Install Drone CLI]**

    Drone is a Continuous Integration platform built on container technology.
    Every build is executed inside an ephemeral Docker container,
    giving developers complete control over their build environment with guaranteed isolation.

* **[Installing Terraform]**

	Terraform is a tool for creating, combining, and managing infrastructure resources across multiple providers. It is required to build ezproxy-cluster for laneweb project.

	**Tip:** for MacOS users:

	```
	brew install terraform
	```

[Install Docker]: https://www.docker.com/products/docker
[Install git-crypt]: https://github.com/AGWA/git-crypt
[Install gcloud]: https://cloud.google.com/sdk/downloads
[Install Drone CLI]:http://readme.drone.io/0.5/install/cli/
[Install Jq]: https://stedolan.github.io/jq/
[Install Yq]: https://github.com/kislyuk/yq#installation
[Installing Terraform]: https://www.terraform.io/intro/getting-started/install.html
[Install AWS CLI]: https://github.com/aws/aws-cli
[Install GPG Tools]: https://www.gnupg.org/software/tools.html
[Install kubectl]: https://kubernetes.io/docs/tasks/kubectl/install/
[Install gsutil]: https://cloud.google.com/storage/docs/gsutil_install