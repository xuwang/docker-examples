include envs
export

help: ## this info
	@# adapted from https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo '_________________'
	@echo '| Make targets: |'
	@echo '-----------------'
	@cat Makefile | grep -E '^[a-zA-Z_-]+:.*?## .*$$' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clone: repos.txt init
	@grep -v '^#' repos.txt | grep https | \
		while read -r line; do \
			domain=`echo $$line | cut -s -d'/' -f 3`; \
			repo=`echo $$line | cut -s -d'/' -f 4-5`; \
			checkout_dir=${SUB_PROJECTS}/`echo $$line | cut -s -d'/' -f 5`; \
			if [ ! -z $$domain ] && [ ! -e $$checkout_dir ]; \
			then \
				echo Clone git@$$domain:$$repo to ${SUB_PROJECTS}/$$checkout_dir; \
				git clone git@$$domain:$$repo $$checkout_dir; \
			else \
				echo Already cloned under $$checkout_dir. Please use 'make update' to update the sub-projects ;\
			fi; \
		done

update: init ## git pull on all sub-projects
	@cd ${SUB_PROJECTS} ; \
	for dir in `ls`; do \
		if [ -d $$dir/.git ]; \
		then \
			echo Check git pull $$dir; \
			cd $$dir; git fetch; git pull; cd ..; \
		fi; \
	done

status: init ## git status on all sub-projects
	@cd ${SUB_PROJECTS} ; \
	for dir in `ls`; do \
		if [ -d $$dir ]; \
		then \
			echo Check git status $$dir; \
			cd $$dir; git status; cd ..; \
		fi; \
	done

clean: ## remove all local sub-projects
	@read -p "This will delete all the local repos, are you sure? [YES/NO] " ; \
	@if [ "$$REPLY" = 'YES' ] ; \
	then \
		echo "rm -rf ${SUB_PROJECTS}" ; \
		rm -rf ${SUB_PROJECTS} ; \
	else \
		echo "do nothing" ; \
	fi

init:
	@mkdir -p ${SUB_PROJECTS}

unlock: ## unlock all the git-crypt protected secrets
	@for dir in ${GIT_CRYPT_REPOS}; do \
		( cd ${SUB_PROJECTS}/$$dir; \
		  if [ ! -f .git/git-crypt/keys/default ]; \
		  then \
			git-crypt unlock; \
		  else \
			echo $$dir already unlockded; \
		  fi; \
		 ); \
	done

lock: ## lock all the git-crypt protected secrets
	@for dir in ${GIT_CRYPT_REPOS}; do \
		( cd ${SUB_PROJECTS}/$$dir; git-crypt lock; ); \
	done

.PHONY: help clean clone status update init unlock lock
