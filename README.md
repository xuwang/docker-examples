# [Docker] Usage Examples


* [my-vault]: a repo that keeps secrets and protected by git-crypt
* [docker-volumes]: use shared volumes between data producer and consumer
* [docker-git-sync]: sync content from a git repo in containers
* [docker-caddy]: an automatic HTTPS web with Caddy
* [docker-gcsfuse]: use GCS bucket as fuse filesys in containers
* [docker-s3fuse]: use s3 bucket as fuse filesys in containers
* [docker-caddy-hugo]: build and run static website easy and fast with docker
* [docker-registry]: a secure private docker registry with authn/authz
* [docker-keycloak]: keycloak identity and access management in containers
* [docker-slap]: a ldap proxy in container
* [docker-apache-openidc]: apache server with mod_auth_openidc and demo with keycloak openidc client config

## Tools and Utilities We Use

See [Tools Doc] for tools installation info, e.g. [Docker], [Git-crypt] etc.

Basic understanding on Git, Docker, Bash, and Make is assumed.

### Get All Docker Usage Example Repos

##### Clone [docker-examples] to local:

```
$ git clone git@code.stanford.edu:xuwang/docker-examples.git
```

##### To clone all the related repos to local:

```
$ cd docker-examples
$ make clone
```
It will clone all the example repos listed above to _sub-projects_ dir.

Go to any of sub-projects and try out specific usage examples by following the README in that repo.

##### To update all the cloned local repos from upstream:

```
$ make update
```

[Tools Doc]: tools.md
[docker-examples]: https://code.stanford.edu/xuwang/docker-examples
[my-vault]: https://code.stanford.edu/xuwang/my-vault
[docker-volumes]: https://code.stanford.edu/xuwang/docker-volumes
[docker-git-sync]: https://code.stanford.edu/xuwang/docker-git-sync
[docker-caddy]: https://code.stanford.edu/xuwang/docker-caddy
[docker-gcsfuse]: https://code.stanford.edu/xuwang/docker-gcsfuse
[docker-s3fuse]: https://code.stanford.edu/xuwang/docker-s3fuse
[docker-caddy-hugo]: https://code.stanford.edu/xuwang/docker-caddy-hugo
[docker-registry]: https://code.stanford.edu/xuwang/docker-registry
[Docker]:https://github.com/moby/moby
[Git-crypt]: https://github.com/AGWA/git-crypt
[docker-keycloak]: https://code.stanford.edu/xuwang/docker-keycloak
[docker-slap]: https://code.stanford.edu/xuwang/docker-slap
[docker-apache-openidc]: https://code.stanford.edu/xuwang/docker-apache-openidc
